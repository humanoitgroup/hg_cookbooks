default["hg_rails"]["git_aware_prompt"] = {}
default["hg_rails"]["git_aware_prompt"]["ps1"] = "\"\\u@\\h:\\w\\[$bldgrn\\]\\$git_branch\\[$txtred\\]\\$git_dirty\\[$txtrst\\]\\$ \""