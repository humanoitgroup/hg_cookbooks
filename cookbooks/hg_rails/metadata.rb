name             'hg_rails'
maintainer       'HG'
license          'All rights reserved'
description      'Installs/Configures rails development stack'
version          '1.0'

depends "rvm", '0.9.2'